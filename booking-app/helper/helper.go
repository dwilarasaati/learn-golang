//menginisialisasi package helper
package helper

//mengimpor package lain yang akan dipakai
import (
	//mengimpor package strings (package untuk mengolah string)
	"strings"
)

//membuat function ValidateUserInput dengan beberapa parameter dan mengembalikan beberapa variabel dengan tipe data boolean
func ValidateUserInput(firstName string, lastName string, email string, userTickets uint, remainingTickets uint) (bool, bool, bool) {

	//deklarasi variable isValidName untuk mengecek apakah panjang dari firstName dan lastName lebih dari 2 karakter
	isValidName := len(firstName) >= 2 && len(lastName) >= 2

	//deklarasi variable isValidEmail untuk mengecek apakah email yang dimasukkan memiliki '@'
	isValidEmail := strings.Contains(email, "@")

	//deklarasi variable isValidTicketNumber untuk mengecek apakah jumlah tiket yang diinput bernilai positif dan kurang atau sama dengan jumlah tiket yang tersisa
	isValidTicketNumber := userTickets > 0 && userTickets <= remainingTickets

	//mengembalikan nilai dari variable isValidName, isValidEmail, isValidTicketNumber
	return isValidName, isValidEmail, isValidTicketNumber
}
