//menginisialisasi default package
package main

//mengimpor package lain yang akan dipakai
import (
	//mengimpor package helper dari module booking-app
	"booking-app/helper"
	//mengimpor package fmt (package untuk formatting input & output I/O)
	"fmt"
	//mengimpor package sync (package untuk sinkronisasi seperti concurrency)
	"sync"
	//mengimpor package time (package untuk mengukur dan memanipulasi waktu)
	"time"
)

//mendeklarasi variable global conferenceName(nama konferensi yang akan dibooking)
var conferenceName = "Go Conference"

//mendeklarasi variable global remainingTicket(jumlah tiket yang tersisa) dengan tipe data uint
var remainingTickets uint = 50

//mendeklarasi variable global bookings dengan membuat slices userData dengan element 0
var bookings = make([]UserData, 0)

//mendeklarasi variable konstanta global conferenceTickets(jumlah tiket awal yang tersedia)
const conferenceTickets int = 50

//mendeklarasi struct untuk menyimpan variabel dengan tipe data yang berbeda
type UserData struct {
	//mendeklarasi variabel firstName(nama depan pembeli) dengan tipe data string
	firstName string
	//mendeklarasi variabel lastName(nama belakang pembeli) dengan tipe data string
	lastName string
	//mendeklarasi variabel email(email pembeli) dengan tipe data string
	email string
	//mendeklarasi variabel numberOfTickets(jumlah tiket yang dibeli) dengan tipe data uint
	numberOfTickets uint
}

//mendeklarasi variable wg(waitgroup) untuk menunggu sampai function tertentu dijalankan(goroutine)
var wg = sync.WaitGroup{}

//menjalankan function main(utama) untuk menjalankan program
func main() {

	//memanggil function greetUsers(kalimat pembuka)
	greetUsers()

	//menerima nilai return yang dihasilkan saat memanggil function getUserInput
	firstName, lastName, email, userTickets := getUserInput()
	//menerima nilai return yang dihasilkan saat memanggil function validateUserInput dengan package helper dengan memasukkan beberapa variable sebagai parameter
	isValidName, isValidEmail, isValidTicketNumber := helper.ValidateUserInput(firstName, lastName, email, userTickets, remainingTickets)

	//membuat percabangan if else dengan kondisi 1 yaitu mengecek apakah nama, email, dan jumlah tiket yang dimasukkan adalah valid
	if isValidName && isValidEmail && isValidTicketNumber {

		//memanggil function bookTicket(memesan tiket) dengan memasukkan beberapa variabel sebagai parameter
		bookTicket(userTickets, firstName, lastName, email)

		//menambahkan jumlah goroutine yang dieksekusi
		wg.Add(1)
		//memanggil goroutine function sendTicket(mengirim tiket) dengan memasukkan beberapa variabel sebagai parameter
		go sendTicket(userTickets, firstName, lastName, email)

		/*

			//menampilkan isi dari array bookings secara keseluruhan
			fmt.Printf("The whole array: %v\n", bookings)

			//menampilkan isi dari nilai array bookings yang pertama
			fmt.Printf("The first value: %v\n", bookings[0])

			//menampilkan tipe data dari array
			fmt.Printf("Array type: %T\n", bookings)\

			//menampilkan panjang array
			fmt.Printf("Array length: %v\n", len(bookings))

		*/

		//menerima nilai return yang dihasilkan pada saat memanggil function getFirstNames
		firstNames := getFirstNames()
		//menampilkan nilai dari firstNames (array yang berisi nama depan pembeli)
		fmt.Printf("The first names of bookings are: %v\n", firstNames)

		/*
			//mendeklarasikan variable remainingTickets dengan tipe data boolean
			var noTicketsRemaining bool = remainingTickets == 0

		*/

		//membuat percabangan jika remainingTickets==0 atau habis terjual maka program akan berhenti
		if remainingTickets == 0 {
			//menampilkan pesan bahwa tiket sudah habis terjual
			fmt.Printf("Our conference is booked out. Come back next year.\n")

			/*
				//menghentikan atau tidak melanjutkan program
				break

			*/
		}

		//melanjutkan percabangan if else kondisi 2 jika kondisi pertama tidak terpenuhi
	} else {

		//mengecek kondisi apakah nama yang diinputkan invalid
		if !isValidName {
			//menampilkan pesan feedback jika kondisi benar
			fmt.Println("First name or last name you entered is too short")

		}

		//mengecek kondisi apakah email yang diinputkan invalid
		if !isValidEmail {
			//menampilkan pesan feedback jika kondisi benar
			fmt.Println("Email address you entered doesn't contain @ sign")
		}

		//mengecek kondisi apakah jumlah tiket yang diinputkan invalid
		if !isValidTicketNumber {
			//menampilkan pesan feedback jika kondisi benar
			fmt.Println("Number of tickets you entered is invalid")
		}
	}
	//memberi perintah blocking atau menunggu hingga goroutine selesai
	wg.Wait()
}

//membuat function greetUser(menyapa pengguna)
func greetUsers() {
	//menampilkan secara formatted output kalimat pembuka dengan mengambil nilai dari variabel conferenceName
	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	//menampilkan secara manual informasi jumlah tiket dengan mengambil nilai dari variabel conferenceTickets dan remainingTickets
	fmt.Println("We have total of", conferenceTickets, "tickets and", remainingTickets, "are still available")
	//menampilkan teks biasa
	fmt.Println("Get your tickets here to attend")
}

//membuat function getFirstNames(mendapatkan nama depan pembeli) yang mengembalikan nilai return berupa array
func getFirstNames() []string {

	//mendeklarasikan variabel firstNames sebagai array yang memiliki tipe data string
	firstNames := []string{}

	//membuat looping for dengan range sebanyak yg ada di array bookings
	for _, booking := range bookings {
		//menambahkan elemen firstNames dengan variabel firstName
		firstNames = append(firstNames, booking.firstName)
	}
	//mengembalikan nilai berupa array firstNames
	return firstNames
}

//membuat function getUserInput(mendapatkan input dari user) kemudian mengembalikan beberapa nilai return
func getUserInput() (string, string, string, uint) {
	//mendeklarasikan variable local firstName dengan tipe data string
	var firstName string
	//mendeklarasikan variable local lastName dengan tipe data string
	var lastName string
	//mendeklarasikan variable local email dengan tipe data string
	var email string
	//mendeklarasikan variable local userTickets dengan tipe data uint
	var userTickets uint

	//menampilkan kalimat perintah input firstName kepada user
	fmt.Println("Enter your first name:")
	//mengambil input firstName dari user menggunakan package scan
	fmt.Scan(&firstName)

	//menampilkan kalimat perintah input lastName kepada user
	fmt.Println("Enter your last name:")
	//mengambil input lastName dari user menggunakan package scan
	fmt.Scan(&lastName)

	//menampilkan kalimat perintah input email kepada user
	fmt.Println("Enter your email address:")
	//mengambil input email dari user menggunakan package scan
	fmt.Scan(&email)

	//menampilkan kalimat perintah input jumlah tiket yang ingin dipesan kepada user
	fmt.Println("Enter number of tickets:")
	//mengambil input jumlah tiket yang ingin dipesan dari user menggunakan package scan
	fmt.Scan(&userTickets)

	//mengembalikan beberapa nilai return
	return firstName, lastName, email, userTickets
}

//membuat function bookTicket degan beberapa variable sebagai parameter
func bookTicket(userTickets uint, firstName string, lastName string, email string) {
	//menghitung remainingTickets dengan mengurangi variable userTickets
	remainingTickets = remainingTickets - userTickets

	//memndeklarasikan variable ke dalam properti dari struct yang telah dibuat
	var userData = UserData{
		firstName:       firstName,
		lastName:        lastName,
		email:           email,
		numberOfTickets: userTickets,
	}

	/*
		//membuat map untuk elemen firstName untuk user
		userData["firstName"] = firstName
		//membuat map untuk elemen lastName untuk user
		userData["lastName"] = lastName
		//membuat map untuk elemen email untuk user
		userData["email"] = email
		//membuat map untuk elemen numberOfTickets untuk user dengan mengonversi menjadi format uint
		userData["numberOfTickets"] = strconv.FormatUint(uint64(userTickets), 10)

	*/

	//menambahkan elemen bookings dengan variabel userData
	bookings = append(bookings, userData)
	//menampilkan list dari array bookings
	fmt.Printf("List of bookings %v\n", bookings)

	//menampilkan dengan formatted output kalimat penutup dengan mengambil beberapa variabel yang dibutuhkkan
	fmt.Printf("Thank you %v %v for booking %v tickets You will receive a confirmation email at %v.\n", firstName, lastName, userTickets, email)
	//menampilkan dengan formatted output informasi jumlah tiket yang tersisa dengan mengambil beberapa variabel yang dibutuhkkan
	fmt.Printf("%v tickets remaining for %v\n", remainingTickets, conferenceName)
}

//membuat function sendTicket dengan memasukkan beberapa variable sebagai parameter
func sendTicket(userTickets uint, firstName string, lastName string, email string) {

	//mengatur waktu jeda eksekusi program selama 50 detik
	time.Sleep(50 * time.Second)

	//mendeklarasikan variabel ticket menggunakan sprintf untuk mengubah format menjadi string
	var ticket = fmt.Sprintf("%v tickets for %v %v", userTickets, firstName, lastName)
	//menampilkan penanda
	fmt.Println("#############")
	//menampilkan informasi dengan formatted output dari beberapa variable
	fmt.Printf("Sending ticket:\n %v\nto email address %v\n", ticket, email)
	//menampilkan penanda
	fmt.Println("#############")
	//memberitahu bahwa program telah selesai dan berakhir
	wg.Done()
}
